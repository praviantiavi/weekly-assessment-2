// write function here
const oddEven = (angka) => {
  if(angka % 2 !== 0) {
    return `${angka} is odd number`;
  };
    return `${angka} is even number`
}
// input test
const input1 = 3;
const input2 = 10;

console.log(oddEven(input1)); // output: 3 is odd number
console.log(oddEven(input2));