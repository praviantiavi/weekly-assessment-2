# Task 1: Understanding Javascript

1. What is the difference between Javascript and HTML?
    - JavaScript is an advanced programming language that makes web pages more  interactive and dynamic.
    - HTML is a standard markup language that provides the primary structure of a website. 

2. What is the differenece between var, let, and const?
    - let is a statement declares a block-scoped local variable, optionally initializing it to a value.
    - var is statement declares a function-scoped or globally-scoped variable, optionally initializing it to a value.
    - const are block-scoped, The value of a constant can't be changed through reassignment, and it can't be redeclared.


3. What data types in javascript do you know?
    - string
    - number
    - boolean
    - null
    - undefined
    - object

4. What do you know about function?
    - the main “building blocks” of the program. They allow the code to be called many times without repetition.
    - dengan menggunakan function, kita hanya tulis codenya sekali, lalu bisa dipakai berkali-kali 

5. What do you know about hoisting?
    - Hoisting is JavaScript's default behavior of moving declarations to the top.