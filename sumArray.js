// write function here
const sumArray = (angka) => {
  let total = 0
  for(i = 0; i < angka.length; i++ ) {
    total += angka[i];
  }
  return total
};

// input test
const input1 = [1, 2, 5, 8, 9, 10];
const input2 = [1, 2, 3, 4, 5];

console.log(sumArray(input1)); // output: 35
console.log(sumArray(input2)); // output: 15
